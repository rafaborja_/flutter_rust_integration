import 'package:flutter/material.dart';
import 'dart:ffi';
import 'dart:io';

final DynamicLibrary greeterNative = Platform.isAndroid
    ? DynamicLibrary.open("libgreeter.so")
    : DynamicLibrary.process();

final int Function(int x) rustGreeting = greeterNative
    .lookup<NativeFunction<Int32 Function(Int32)>>("rust_greeting")
    .asFunction();

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final edad = 18;
    final resultPtr = rustGreeting(edad);
    print("Tu año de nacimiento es : $resultPtr");
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Container(
            child: Text('Hello World'),
          ),
        ),
      ),
    );
  }
}
